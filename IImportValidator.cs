﻿using System.Collections.Generic;
using GameLib.Core.Parsers;

namespace GameLib.GoogleDataImport
{
	/// <summary>
	/// validate import (link with [ImportValidator()] attribute)
	/// </summary>
	public interface IImportValidator
	{
		void ValidateValue(Dictionary<string, object> values, IRawTableRow reporter);
	}
}