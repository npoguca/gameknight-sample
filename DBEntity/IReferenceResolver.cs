namespace GameLib.DbFramework.Internal.ReferenceBuilders
{
	internal interface IReferenceResolver
	{
		bool CanResolve(MemberInfoEx refProperty);
		void Resolve(object entity, MemberInfoEx refProperty, ReferenceBuilder builder, int tableIndex);
	}
}