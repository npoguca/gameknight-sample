using System;
using System.Collections.Generic;
using System.Linq;
using GameLib.Core.Parsers;
using GameLib.Core.Utils;

namespace GameLib.GoogleDataImport.Internal
{
	internal static class ImportUtility
	{
		private static readonly List<string> ImportStack = new List<string>(32);

		public static void PushLocation(string location)
		{
			ImportStack.Add(location);
		}

		public static void PopLocation()
		{
			if (ImportStack.Count == 0)
			{
				return;
			}

			ImportStack.RemoveAt(ImportStack.Count - 1);
		}

		public static string CurrentStack => ImportStack.JoinToString("/");

		private static Dictionary<Type, ICustomImporter> _customImporters;

		public static bool TryImport(Type argType, IRawTableRow row, string columnName, ImportOptions options, out object result)
		{
			if (_customImporters == null)
			{
				_customImporters = Types.EnumerateAll(x => TypeOf<ICustomImporter>.Raw.IsAssignableFrom(x) && x.IsClass && !x.IsAbstract)
					.Select(x => (ICustomImporter)Activator.CreateInstance(x))
					.ToDictionary(x => x.CustomType);
			}

			if (!_customImporters.TryGetValue(argType, out var importer))
			{
				result = null;
				return false;
			}

			result = importer.Import(row, columnName, options);
			return true;
		}
	}
}