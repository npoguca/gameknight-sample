﻿using System;

namespace GameLib.GoogleDataImport.Attributes
{
	/// <summary>
	/// constructor used for importing data from google sheets 
	/// </summary>
	[AttributeUsage(AttributeTargets.Constructor)] 
	public class DataImportConstructorAttribute: Attribute
	{
	}
}