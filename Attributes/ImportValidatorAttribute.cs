﻿using System;

namespace GameLib.GoogleDataImport.Attributes
{
	/// <summary>
	/// check value after import. ValidatorType mut be IImportValidator instance 
	/// </summary>
	[AttributeUsage(AttributeTargets.Constructor)] 
	public class ImportValidatorAttribute: Attribute
	{
		public Type ValidatorType { get; }

		public ImportValidatorAttribute(Type validatorType)
		{
			ValidatorType = validatorType;
		}
	}
}