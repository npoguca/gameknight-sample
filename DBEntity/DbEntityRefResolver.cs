using System;
using System.Collections;
using System.Linq;
using GameLib.Core.Reflection;
using GameLib.Core.Utils;
using GameLib.DbFramework.Attributes;
using GameLib.DbFramework.Contracts;
using GameLib.DbFramework.Exceptions;
using GameLib.DbFramework.Types;

namespace GameLib.DbFramework.Internal.ReferenceBuilders
{
	internal class DbEntityRefResolver: IReferenceResolver
	{
		public bool CanResolve(MemberInfoEx refProperty)
		{
			return refProperty.HasAttribute<KeyValueAttribute>() && !refProperty.PropertyType.IsArray;
		}

		public void Resolve(object entity, MemberInfoEx refProperty, ReferenceBuilder builder, int tableIndex)
		{
			var foreignType = refProperty.PropertyType;
			if (foreignType == null || foreignType.IsAbstract || !foreignType.IsClass || !TypeOf<IDbEntity>.Raw.IsAssignableFrom(foreignType))
			{
				throw new DbBuilderException(entity,$"must have type: {nameof(IDbEntity)} derived non-abstract class");
			}

			var ownerType = entity.GetType();
			
			var valueAttr = refProperty.GetAttribute<KeyValueAttribute>();
			if (valueAttr == null)
			{
				throw new DbBuilderException(entity,"must have [KeyValue(\"_memberName\")] attribute");
			}

			object refObj;
			Type entityRefType;

			var valueProperty = ownerType.FindInstancedProperty(valueAttr.ValueName);
			if (valueProperty == null)
			{
				var valueField = ownerType.FindInstancedField(valueAttr.ValueName);
				if (valueField == null)
				{
					throw new DbBuilderException(entity,$"Cannot find property or field in type '{ownerType.FullName}' with name '{valueAttr.ValueName}'");
				}
				
				entityRefType = valueField.FieldType;
				refObj = valueField.GetValue(entity);
			}
			else
			{
				entityRefType = valueProperty.PropertyType;
				refObj = valueProperty.GetValue(entity);
			}
			

			if (entityRefType != TypeOf<DbEntityId>.Raw)
			{
				throw new DbBuilderException(entity,$"ValueRef field '{valueAttr.ValueName}' must be of type '{nameof(DbEntityId)}'");
			}

			var id = (DbEntityId)refObj;
			if (id == DbEntityId.Empty)
			{
				return;
			}

			var foreignTable = builder.DbContext.All(foreignType);

			var result = GetEntity(foreignTable, id);
			if (result == null)
			{
				throw new DbBuilderException(entity,$"Cannot find '{foreignType.Name}' item with id='{id}'");
			}
			
			refProperty.SetValue(entity, result);
		}

		private static IDbEntity GetEntity(IEnumerable foreignTable, DbEntityId id)
		{
			return foreignTable.Cast<IDbEntity>().FirstOrDefault(foreignEntity => foreignEntity.Id == id);
		}
	}
}