﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;

// ReSharper disable InconsistentNaming

namespace GameLib.GoogleDataImport
{
	[SuppressMessage("ReSharper", "NotAccessedField.Global")]
	public class DataImportSettings: ScriptableObject
	{
		public string Google_WebServiceURL;
		public string Google_SpreadsheetKey;
		public string Google_SpreadsheetName;

		public string Google_Password = "change_this";
	}
}