using System;
using System.Collections.Generic;
using System.Linq;
using GameLib.Core.Utils;
using GameLib.DbFramework.Contracts;
using GameLib.DbFramework.Exceptions;
using GameLib.DbFramework.Internal.ReferenceBuilders;
using GameLib.DbFramework.Types;

namespace GameLib.DbFramework.Internal.Context
{
	internal class DbContext : IDbContext
	{
		private readonly Dictionary<Type, IDbCollection> _tables;

		public DbContext(IEnumerable<IDbCollection> collections)
		{
			if (collections == null)
			{
				throw new ArgumentNullException(nameof(collections));
			}
			
			_tables = collections.ToDictionary(x => x.EntityType);

			InitializeReferences();
		}

		private void InitializeReferences()
		{
			var builder = new ReferenceBuilder(this,
				new DbEntityRefResolver(),
				new DbEntityRefArrayResolver(),
				new DbCollectionRefResolver(),
				new InnerClassReferenceResolver(),
				new CalculatedPropertyResolver());

			foreach (var dbCollection in _tables.Values)
			{
				var index = 0;
				foreach (var dbEntity in dbCollection)
				{
					builder.Build(dbEntity, index);
					++index;
				}

			}
		}

		public IEnumerable<IDbCollection> AllTables()
		{
			return _tables.Values;
		}

		public IDbCollection<TEntity> All<TEntity>() where TEntity : class, IDbEntity
		{
			return (IDbCollection<TEntity>)All(TypeOf<TEntity>.Raw);
		}

		public IDbCollection All(Type entryType)
		{
			if (!_tables.TryGetValue(entryType, out var result))
			{
				throw new DbFrameworkException($"Collection for type {entryType.FullName} is not found!");
			}

			return result;
		}

		public TEntity Get<TEntity>(DbEntityId id) where TEntity : class, IDbEntity
		{
			return All<TEntity>().Find(id);
		}

		public bool TryGet<TEntity>(DbEntityId id, out TEntity result) where TEntity : class, IDbEntity
		{
			return All<TEntity>().TryFind(id, out result);
		}

		public TEntity Unique<TEntity>() where TEntity : class, IDbEntity
		{
			var collection = All<TEntity>();
			if (collection.Count != 1)
			{
				throw new DbFrameworkException($"Cannot get unique item {TypeOf<TEntity>.Name}: expected 1 entity, but found {collection.Count}");
			}

			return collection.First();
		}
	}
}