using System;

namespace GameLib.GoogleDataImport.Internal
{
	internal class ImportStack: IDisposable
	{
		public ImportStack(string location)
		{
			ImportUtility.PushLocation(location);
		}
		
		public void Dispose()
		{
			ImportUtility.PopLocation();
		}
	}
}