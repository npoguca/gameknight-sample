using System.Linq;
using System.Reflection;
using GameLib.Core.Reflection;
using GameLib.DbFramework.Exceptions;
using GameLib.DbFramework.Internal.Context;

namespace GameLib.DbFramework.Internal.ReferenceBuilders
{
	internal class ReferenceBuilder
	{
		public DbContext DbContext { get; }
		private readonly IReferenceResolver[] _resolvers;

		public ReferenceBuilder(DbContext dbContext, params IReferenceResolver[] resolvers)
		{
			DbContext = dbContext;
			_resolvers = resolvers;
		}

		public void Build(object entity, int tableIndex)
		{
			if (entity == null)
			{
				return;
			}
			
			foreach (var refProperty in entity.GetType().GetInstanceProperties().Select(ToExInfo))
			{
				ProcessProperty(entity, refProperty, tableIndex);
			}
			foreach (var refProperty in entity.GetType().GetInstanceFields().Select(ToExInfo))
			{
				ProcessProperty(entity, refProperty, tableIndex);
			}
		}

		private void ProcessProperty(object entity, MemberInfoEx refProperty, int tableIndex)
		{
			var resolver = _resolvers.FirstOrDefault(x => x.CanResolve(refProperty));
			if (resolver == null)
			{
				return;
			}

			if (!refProperty.CanRead || !refProperty.CanWrite)
			{
				throw new DbBuilderException(entity, refProperty, "Property must have 'get' and 'set' accessors");
			}

			resolver.Resolve(entity, refProperty, this, tableIndex);
		}

		private static MemberInfoEx ToExInfo(PropertyInfo arg)
		{
			return new MemberInfoEx(arg);
		}
		
		private static MemberInfoEx ToExInfo(FieldInfo arg)
		{
			return new MemberInfoEx(arg);
		}
	}
}