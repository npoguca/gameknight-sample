using System;
using GameLib.Core.Json;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameLib.DbFramework.Types
{
	/// <summary>
	/// ID for database entity
	/// </summary>
	[Serializable, InlineProperty, JsonConverter(typeof(GenericIdConverter<DbEntityId>))]
	public struct DbEntityId : IComparable<DbEntityId>
	{
		/// <summary>
		/// empty value for ID
		/// </summary>
		public static DbEntityId Empty = default;
		
		/// <summary>
		/// ID for single-entry tables
		/// </summary>
		public static DbEntityId Single = new DbEntityId("$_single"); 
		
		[SerializeField, HideLabel, InlineProperty] private string _id;

		public bool IsEmpty => _id.IsNullOrEmpty();

		public DbEntityId(string id)
		{
			if (id.IsNullOrEmpty())
			{
				throw new ArgumentNullException(nameof(id));
			}
			
			_id = id;
		}

		public bool Equals(string id)
		{
			return _id == id;
		}

		public readonly bool Equals(DbEntityId other)
		{
			return _id == other._id;
		}

		public bool EqualsIgnoreCase(DbEntityId other)
		{
			return _id.Equals(other._id, StringComparison.InvariantCultureIgnoreCase);
		}

		public override bool Equals(object obj)
		{
			return obj is DbEntityId other && Equals(other);
		}

		public override int GetHashCode()
		{
			return _id != null ? _id.GetHashCode() : 0;
		}

		public int CompareTo(DbEntityId other)
		{
			return string.Compare(_id, other._id, StringComparison.Ordinal);
		}

		public static bool operator ==(DbEntityId a, DbEntityId b)
		{
			return a._id == b._id;
		}

		public static bool operator !=(DbEntityId a, DbEntityId b)
		{
			return !(a == b);
		}

		public static bool operator ==(DbEntityId a, string b)
		{
			return a._id == b;
		}

		public static bool operator !=(DbEntityId a, string b)
		{
			return !(a == b);
		}

		public override string ToString()
		{
			return _id;
		}
		
		/// <summary>
		/// converter for LINQ selectors.
		/// usage: dbArray = stringArray.SelectToArray(DbEntityId.FromString)
		/// </summary>
		public static DbEntityId FromString(string s)
		{
			return new DbEntityId(s);
		}
	}
}