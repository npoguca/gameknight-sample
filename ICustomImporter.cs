using System;
using GameLib.Core.Parsers;

namespace GameLib.GoogleDataImport
{
	public interface ICustomImporter
	{
		Type CustomType { get; }
		object Import(IRawTableRow row, string columnName, ImportOptions options);
	}
}