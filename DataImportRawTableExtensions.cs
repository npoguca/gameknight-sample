﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using GameLib.Core.Parsers;
using GameLib.Core.Reflection;
using GameLib.Core.Utils;
using GameLib.GoogleDataImport.Attributes;
using GameLib.GoogleDataImport.Internal;
using UnityEngine;

namespace GameLib.GoogleDataImport
{
	// ReSharper disable once UnusedType.Global
	public static class DataImportRawTableExtensions
	{
		/// <summary>
		/// create new instance and initialize it with [DataImportConstructor]  
		/// </summary>
		public static T LoadObject<T>(this IRawTableRow row, ImportOptions options = null)
		{
			return LoadObject<T>(row, TypeOf<T>.Raw, options);
		}

		/// <summary>
		/// create new instance and initialize it with [DataImportConstructor]  
		/// </summary>
		public static T LoadObject<T>(this IRawTableRow row, Type objectType, ImportOptions options = null)
		{
			var obj = LoadObject(row, objectType, options);

			T result;
			try
			{
				result = (T)obj;
			}
			catch (Exception e)
			{
				if (e is TargetInvocationException te && te.InnerException != null)
				{
					e = te.InnerException;
				}

				Debug.LogError(e);
				throw row.MakeError(e.Message);
			}

			return result;
		}

		/// <summary>
		/// create new instance and initialize it with [DataImportConstructor]  
		/// </summary>
		private static object LoadObject(this IRawTableRow row, Type objectType, ImportOptions options = null)
		{
			using (var _ = new ImportStack(objectType.Name))
			{
				try
				{
					if (objectType.IsAbstract)
					{
						throw row.MakeError($"Class {objectType.Name} is abstract! May be ICustomImporter missing for this type?");
					}

					if (objectType.IsInterface)
					{
						throw row.MakeError($"Class {objectType.Name} is interface! May be ICustomImporter missing for this type?");
					}

					var constructors = objectType.GetConstructors();

					ConstructorInfo ctor;
					if (constructors.Length == 1)
					{
						ctor = constructors.First();
					}
					else if (constructors.Length > 1)
					{
						ctor = constructors.FirstOrDefault(x => x.HasAttribute<DataImportConstructorAttribute>());
						if (ctor == null)
						{
							throw row.MakeError(
								$"Class {objectType.Name} have multiple constructors. One of them must have [DataImportConstructor] attribute!");
						}
					}
					else
					{
						throw row.MakeError($"Class {objectType.Name} must have at least one public constructor!");
					}

					var paramValues = new object[ctor.GetParameters().Length];
					for (var index = 0; index < ctor.GetParameters().Length; index++)
					{
						var paramInfo = ctor.GetParameters()[index];

						if (options != null && options.ExtraParams.TryGetValue(GetColumnName(paramInfo), out var paramValue))
						{
							paramValues[index] = paramValue;
							continue;
						}

						if (TypeOf<IRawTableRow>.Raw.IsAssignableFrom(paramInfo.ParameterType))
						{
							paramValues[index] = row;
							continue;
						}

						var arrayAttr = paramInfo.GetAttribute<ImportArrayAttribute>();

						object value;

						if (arrayAttr?.StartIndex != null)
						{
							var columnName = arrayAttr.ColumnName;

							if (!columnName.Contains("{0}"))
							{
								throw row.MakeError(
									$"Class {objectType.Name}: constructor parameter '{paramInfo.Name}' with [ImportArray] must have column name with '{{0}}' placeholder!");
							}

							var arrayType = paramInfo.ParameterType;
							if (!arrayType.IsArray)
							{
								throw row.MakeError(
									$"Class {objectType.Name}: constructor parameter '{paramInfo.Name}' with [ImportArray] must have array type!");
							}

							value = ImportMultiColumnArray(row, arrayAttr, arrayType, options);
						}
						else if (arrayAttr != null)
						{
							var arrayType = paramInfo.ParameterType;
							if (!arrayType.IsArray)
							{
								throw row.MakeError(
									$"Class {objectType.Name}: constructor parameter '{paramInfo.Name}' with [ImportArray] must have array type!");
							}

							value = ImportOneCellArray(row, arrayAttr, arrayType);
						}
						else
						{
							var columnName = GetColumnName(paramInfo);

							Debug.Assert(!columnName.IsNullOrEmpty(), ImportUtility.CurrentStack);

							if (paramInfo.ParameterType.IsArray)
							{
								if (options?.RowList == null)
								{
									throw row.MakeError(
										$"Class {objectType.Name}: array {paramInfo.ParameterType.Name} can be imported only from {nameof(ItemRowList)} class or with [ImportArray] attribute!");
								}

								value = ImportRowsArray(columnName, paramInfo.ParameterType, options.RowList, options, paramInfo.HasDefaultValue,
									paramInfo.DefaultValue);
							}
							else
							{
								value = LoadValue(row, options, columnName, paramInfo.ParameterType, paramInfo.HasDefaultValue, paramInfo.DefaultValue);
							}
						}

						paramValues[index] = value;
					}

					var validatorAttr = ctor.GetAttribute<ImportValidatorAttribute>();

					if (validatorAttr != null)
					{
						var obj = Activator.CreateInstance(validatorAttr.ValidatorType);
						if (!(obj is IImportValidator validator))
						{
							throw row.MakeError(
								$"Class {objectType.Name}: constructor '{objectType.Name}' has [ImportValidator] - validator must have IImportValidator type!");
						}

						var values = ctor.GetParameters()
							.Zip(paramValues, (p, v) => new {p.Name, v})
							.ToDictionary(x => x.Name, x => x.v);

						validator.ValidateValue(values, row);
					}

					var result = ctor.Invoke(paramValues);
					return result;
				}
				catch (Exception e)
				{
					if (e is TargetInvocationException te && te.InnerException != null)
					{
						e = te.InnerException;
					}

					var err = $"[Importing object {ImportUtility.CurrentStack}] {e.Message}";
					Debug.LogError(err);
					Debug.LogError(e);
					throw row.MakeError(err);
				}
			}

			string GetColumnName(ParameterInfo paramInfo)
			{
				var columnAttr = paramInfo.GetAttribute<ColumnNameAttribute>();
				var columnName = columnAttr?.Name ?? paramInfo.Name;

				if (options != null && options.RemapParams.TryGetValue(columnName, out var remapped))
				{
					columnName = remapped;
				}

				return columnName;
			}
		}

		public static T LoadValue<T>(this IRawTableRow row, string columnName, ImportOptions options = null)
		{
			return (T)LoadValue(row, options, columnName, TypeOf<T>.Raw, false, null);
		}
		
		public static T LoadValue<T>(this IRawTableRow row, string columnName, object defaultValue, ImportOptions options = null)
		{
			return (T)LoadValue(row, options, columnName, TypeOf<T>.Raw, true, defaultValue);
		}

		private static object LoadValue(IRawTableRow row, ImportOptions options, string columnName, Type parameterType, bool hasDefault, object defaultValue)
		{
			//Debug.Log($"{columnName} ({parameterType.Name}) -- {options.RemapParams.Select(x => $"{x.Key}={x.Value}").JoinToString()}");

			if (ImportUtility.TryImport(parameterType, row, columnName, options, out var value))
			{
				if (value == null)
				{
					value = hasDefault ? defaultValue : throw row.MakeError($"'{columnName}' column not found or empty!'");
				}
			}
			else if (hasDefault && row.IsEmpty(columnName))
			{
				value = defaultValue;
			}			
			else
			{
				value = row.GetValue(columnName, parameterType);
			}

			return value;
		}

		private static object ImportRowsArray(string columnName, Type arrayType, ItemRowList rowList, ImportOptions importOptions, bool hasDefault,
			object defaultValue)
		{
			Debug.Assert(arrayType.IsArray);
			var itemType = arrayType.GetElementType();

			var items = new ArrayList();

			foreach (var row in rowList)
			{
				var val = LoadValue(row, importOptions, columnName, itemType, true, null);

				if (val != null)
				{
					items.Add(val);
				}
			}

			if (items.Count == 0 && !hasDefault)
			{
				rowList.ItemRow.MakeError($"Column {columnName} must be non-empty!");
			}

			return items.ToArray(itemType);
		}

		private static object ImportOneCellArray(IRawTableRow row, ImportArrayAttribute arrayAttr, Type arrayType)
		{
			try
			{
				var items = new ArrayList();

				var itemType = arrayType.GetElementType();
				Debug.Assert(itemType != null);

				var columnName = arrayAttr.ColumnName;
				var hasValue = !row.IsEmpty(columnName);
				var requiredCount = arrayAttr.MinCount;

				if (!hasValue)
				{
					if (requiredCount > 0)
					{
						throw row.MakeError($"{columnName} column must be non empty!");
					}

					return items.ToArray(itemType);
				}

				var valueStr = row.GetString(columnName);

				var values = valueStr.Split(new[] {',', '\n', ';'}, StringSplitOptions.RemoveEmptyEntries);

				if (values.Length < arrayAttr.MinCount)
				{
					throw row.MakeError($"{columnName} at least {arrayAttr.MinCount} values required, but found only {values.Length}!");
				}

				for (var index = 0; index < values.Length; index++)
				{
					var v = values[index].Trim();

					if (itemType == TypeOf<string>.Raw)
					{
						items.Add(v);
					}
					else if (itemType == TypeOf<int>.Raw)
					{
						items.Add(int.Parse(v));
					}
					else if (itemType == TypeOf<float>.Raw)
					{
						items.Add(float.Parse(v.Replace(',', '.'), NumberStyles.Float, CultureInfo.InvariantCulture));
					}
					else if (itemType == TypeOf<double>.Raw)
					{
						items.Add(double.Parse(v.Replace(',', '.'), NumberStyles.Float, CultureInfo.InvariantCulture));
					}
					else if (itemType.IsEnum)
					{
						items.Add(Enums.ToEnum(itemType, v));
					}
					else
					{
						var valueRow = RawTableRowExtensions.MakeRow(new Dictionary<string, string>() {{columnName, v}}, row.Location);
						items.Add(LoadValue(valueRow, null, columnName, itemType, false, null));
					}
				}

				return items.ToArray(itemType);
			}
			catch (Exception e)
			{
				if (e is TargetInvocationException te && te.InnerException != null)
				{
					e = te.InnerException;
				}

				Debug.LogError(e);
				throw row.MakeError(e.Message);
			}
		}

		private static object ImportMultiColumnArray(IRawTableRow row, ImportArrayAttribute arrayAttr, Type arrayType, ImportOptions options)
		{
			try
			{
				var items = new ArrayList();

				var itemType = arrayType.GetElementType();
				Debug.Assert(itemType != null);

				var requiredCount = arrayAttr.MinCount;
				for (var i = 0; i < arrayAttr.MaxCount; i++)
				{
					var columnName = string.Format(arrayAttr.ColumnName, i + arrayAttr.StartIndex);

					var hasValue = !row.IsEmpty(columnName);

					if (requiredCount > 0)
					{
						if (!hasValue)
						{
							throw row.MakeError($"{columnName} column must be non empty!");
						}

						--requiredCount;
					}
					else if (!hasValue)
					{
						break;
					}

					var value = LoadValue(row, options, columnName, itemType, false, null);

					items.Add(value);
				}

				return items.ToArray(itemType);
			}
			catch (Exception e)
			{
				if (e is TargetInvocationException te && te.InnerException != null)
				{
					e = te.InnerException;
				}

				Debug.LogError(e);
				throw row.MakeError(e.Message);
			}
		}

		public static Type GetType<TBaseType>(this IRawTableRow row, string columnWithType, string prefix = "", string suffix = "", bool optional = false)
		{
			if (row.IsEmpty(columnWithType))
			{
				return null;
			}

			var typeName = row.GetString(columnWithType);
			if (!prefix.IsNullOrEmpty() && !typeName.StartsWith(prefix))
			{
				typeName = prefix + typeName;
			}

			if (!suffix.IsNullOrEmpty() && !typeName.EndsWith(suffix))
			{
				typeName += suffix;
			}

			var type = TypeCache<TBaseType>.FirstOrDefault(typeName);

			if (type == null)
			{
				if (optional)
				{
					return null;
				}

				throw row.MakeError($"Unsupported type '{typeName}'");
			}

			return type;
		}
	}
}