﻿using System;

namespace GameLib.GoogleDataImport.Attributes
{
	/// <summary>
	/// import array from one or set of columns 
	/// </summary>
	[AttributeUsage(AttributeTargets.Parameter)] 
	public class ImportArrayAttribute: Attribute
	{
		public string ColumnName { get; }
		public int MinCount { get; }

		public int? StartIndex { get; }
		public int? MaxCount { get; }

		/// <summary>
		/// import array from set of columns 
		/// 	example:
		/// 	ImportArray("ForceSetManager_{0}", 1, 0, 4)] string[] forceDropManagers
		/// </summary>
		/// <param name="columnName">base column name. Must have {0} format specifier for index</param>
		/// <param name="startIndex">Start index to read from</param>
		/// <param name="minCount">minimum column count. May be 0 for optional arrays</param>
		/// <param name="maxCount">maximum column count</param>
		public ImportArrayAttribute(string columnName, int startIndex, int minCount, int maxCount)
		{
			ColumnName = columnName;
			StartIndex = startIndex;
			MinCount = minCount;
			MaxCount = maxCount;
		}

		/// <summary>
		/// import array from one column (comma- or newline- separated)
		/// 	example:
		/// 	ImportArray("ForceSetManagers")] string[] forceDropManagers
		/// </summary>
		/// <param name="columnName">column name</param>
		/// <param name="minCount">minimum column count. May be 0 for optional arrays</param>
		public ImportArrayAttribute(string columnName, int minCount = 0)
		{
			ColumnName = columnName;
			StartIndex = null;
			MaxCount = null;
			
			MinCount = minCount;
		}
	}
}